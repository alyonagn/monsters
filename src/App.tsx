import './App.css';
import { ChangeEventHandler, useEffect, useState } from 'react';
import CardList from './components/card-list/card-list.component';
import SearchBox from './components/search-box/search-box.component';
import { getData } from './utils/data.utils';

export type Monster = {
  id: string;
  email: string;
  name: string;
}

const App = () => {
  const [searchField, setSearchField] = useState('');
  const [monsters, setMonsters] = useState<Monster[]>([]);
  const [filteredMonsters, setFilteredMonsters] = useState<Monster[]>([]);

  const fetchMonsters = async () => {
    return await getData<Monster[]>('https://jsonplaceholder.typicode.com/users')
  }

  useEffect(() => {
    const filteredMonsters = monsters.filter((monster) =>
      monster.name.toLowerCase().includes(searchField)
    );
    setFilteredMonsters(filteredMonsters);
  }, [monsters, searchField]);

  useEffect(() => {
    const initializeMonsters = async () => {
      const monsters = await fetchMonsters();
      setMonsters(monsters);
    }
    initializeMonsters();
  }, []);

  const onSearchMonsters: ChangeEventHandler<HTMLInputElement> = (e) => {
    const input = e.target.value;
    setSearchField(input.toLowerCase());
  };

  return (
    <div className="App">
      <h1 className="app-title">Monsters Rolodex</h1>
      <SearchBox
        onChangeHandler={onSearchMonsters}
        placeholder="search monsters"
        className="monsters-search-box"
      />
      <CardList monsters={filteredMonsters} />
    </div>
  );
};

export default App;
