import { ChangeEventHandler } from 'react';
import './search-box.styles.css';

interface ISearchBoxProps {
    className: string;
    placeholder: string;
    onChangeHandler: ChangeEventHandler<HTMLInputElement>;
}

const SearchBox = ({ onChangeHandler, placeholder, className }: ISearchBoxProps) => {
        return (
            <input
                className={`search-box ${className}`}
                type="search"
                placeholder={placeholder}
                onChange={onChangeHandler}
            />
        );
}

export default SearchBox;